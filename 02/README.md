# Неделя 2
Для всех:  
1 глава учебника Public Program Evaluation: A statistical guide (Лангбайн, Фельбингер)  
Для любознательных:  
1 глава учебника Statistical Tools for Program Evaluation (Жосселин, Ле Мо)  
Для тех, кто не был на лекции:  
1 глава учебника Program Evaluation: Alternative Approaches and Practical Guidelines (Фитцпатрик, Сандерс, Вортен)