# ppa301

Public Policy Analysis 2020

## Зеркало на OneDrive

[Перейдите по ссылке](https://ranepa-my.sharepoint.com/:f:/g/personal/shalaev-ne_ranepa_ru/EsF5WJYQfwBLvwdETeG9hFsBkhOvP9TbfQWUc9rOv_Qmjw)

## Неделя 1
2 глава из учебника Public Policy Analysis Уильяма Данна  
2 глава из учебника Policy Analysis Девида Ваймера

## Неделя 2
Для всех:  
1 глава учебника Public Program Evaluation: A statistical guide (Лангбайн, Фельбингер)  
Для любознательных:  
1 глава учебника Statistical Tools for Program Evaluation (Жосселин, Ле Мо)  
Для тех, кто не был на лекции:  
1 глава учебника Program Evaluation: Alternative Approaches and Practical Guidelines (Фитцпатрик, Сандерс, Вортен)

## Неделя 3

Аудио-лекция + задания на семинар (см. каталог 03).

## Неделя 4

Аудио-семинар + задания на семинар (см. каталог 04).
Аудио-лекция (см. каталог 04).

## Неделя 5
Аудио-семинар + задания на семинар (см. каталог 05).

## Неделя 6
Аудио-лекция + задания на семинар (см. каталог 06).

И т.д. по каталогам

## Итоговые задания

Final_assignments.pdf