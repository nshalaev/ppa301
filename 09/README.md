# Неделя 9

## Лекция

Аудио: L08.mp3

Презентация: L08.pptx

## Семинар

### Для чтения

Multiple_criteria_decision_making_techniques_and_their_applications_a_review_of_the_literature_from_2000_to_2014.pdf 

Обзор литературы и методов по MCDM. Не пугайтесь размера, большую часть статьи занимает подробное указание статей и использованных в них методов.

### По желанию

Zeleny1998.pdf — M. Zeleny, Multiple criteria decision making: eight concepts of optimality

Zeleny2011.pdf — M. Zeleny, Multiple Criteria Decision Making (MCDM): From Paradigm Lost to Paradigm Regained

Статьи носят математический характер, но интересные.

### Вопрос для семинара

1. Какие проблемы возникают при окончательном выборе лучшего из разработанных решений проблемы? (2б)  
2. Какие методы помогают бороться с этими проблемами? (2б)  